import time
import glob, random, sys, vlc
import RPi.GPIO as GPIO
import os, time, random
from Adafruit_CharLCD import Adafruit_CharLCD as LCD

'''Lecteur VLC'''
instance = vlc.Instance()
player = instance.media_player_new()
player.audio_set_volume(40)

'''Playlist'''
playlist = os.listdir('.')
random.shuffle(playlist)

current_track = 0
media = instance.media_new(playlist[current_track])
player.set_media(media)

'''GPIO'''
GPIO.setmode(GPIO.BCM)
STOP_BUTTON=27
NEXT_BUTTON=17
UP_BUTTON=14
DOWN_BUTTON=15
RESET_BUTTON=4
SWITCH_BUTTON=26

GPIO.setup(STOP_BUTTON, GPIO.IN)
GPIO.setup(UP_BUTTON, GPIO.IN)
GPIO.setup(DOWN_BUTTON, GPIO.IN)
GPIO.setup(RESET_BUTTON, GPIO.IN)
GPIO.setup(NEXT_BUTTON, GPIO.IN)
GPIO.setup(SWITCH_BUTTON, GPIO.IN)

'''Functions Affichage'''
def DisplaySongInfos():
 global current_track
 number, title, guest, _ = playlist[current_track].replace('_', ' ').split('.')

 lcd.setCursor(0, 0)
 lcd.message(f'{number} - {title}')
 lcd.setCursor(0, 1)
 lcd.message(f'{guest}')

 if len(f'{number} - {title}') > 16:
  Line0 = f'{number} - {title}    '
 else:
  Line0 = f'{number} - {title}'

 if len(f'{guest}') > 16:
  Line1 = f'{guest}    '
 else:
  Line1 = f'{guest}'

  return Line0, Line1
  
def scrollOneLine(line, string):
 if len(string) > 16:
  string = string + "      "
  lcd.setCursor(0, line)
  lcd.message(string)

'''Functions Boutons'''
def play_pause(channel):
 global current_track
 if player.is_playing():
  player.pause()
  lcd.clear()
  lcd.message('Pause')
  
 else:
  player.play()
  lcd.clear()
  DisplaySongInfos()
 
def next_track(channel):
 global current_track
 global k
 k = 0
 if current_track + 1 < len(playlist):
  current_track = current_track + 1
 else:
  current_track = 0
 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 DisplaySongInfos()

def reset_track(channel):
 global current_track
 
 lcd = LCD()
 lcd.begin(16,2)
 lcd.clear()

 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 DisplaySongInfos()

def shutdown(channel):
 lcd.clear()
 lcd.message('Arret en cours')
 player.pause()
 time.sleep(2)
 lcd.clear()
 GPIO.cleanup()
 os.system('sudo shutdown -h now')
 
def volume_up(channel):
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = min(1000, current_volume + 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.message(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 DisplaySongInfos()

def volume_down(channel):
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = max(0, current_volume - 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.message(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 DisplaySongInfos()
 
 
'''Function & GPIO'''
GPIO.add_event_detect(STOP_BUTTON, GPIO.FALLING, callback = play_pause, bouncetime = 1000)
GPIO.add_event_detect(UP_BUTTON, GPIO.FALLING, callback = volume_up, bouncetime = 1000)
GPIO.add_event_detect(DOWN_BUTTON, GPIO.FALLING, callback = volume_down, bouncetime = 1000)
GPIO.add_event_detect(RESET_BUTTON, GPIO.FALLING, callback = reset_track, bouncetime = 1000)
GPIO.add_event_detect(NEXT_BUTTON, GPIO.FALLING, callback = next_track, bouncetime = 1000)
GPIO.add_event_detect(SWITCH_BUTTON, GPIO.FALLING, callback = shutdown, bouncetime = 1000)

'''Boucle principale'''

lcd = LCD()
lcd.begin(16,2)
lcd.clear()


try:
 k = 0
 player.play()
 starttime = time.time()
 while True:
  if k in [0, 0.5]:
   Line0, Line1 = DisplaySongInfos()
  time.sleep(0.5)
  if len(Line0) > 16:
   Line0 = Line0[1:] + Line0[0]
  if len(Line1) > 16:
   Line1 = Line1[2:] + Line1[0]

  scrollOneLine(0, Line0)
  scrollOneLine(1, Line1)
  length = player.get_length()/1000
  timelocal = time.time()

  
  if k >= int(length):
   k = 0
  
  #if not player.is_playing() and k == 0:
  if time.time() >= starttime + length and k == 0:
   next_track(0)
   starttime = time.time()
   k = 0
   
  if player.is_playing():
   print('+', k)
   k = k+0.5

 
except KeyboardInterrupt:
 GPIO.cleanup()
 player.stop()
 lcd.clear()
