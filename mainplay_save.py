import time
import glob, random, sys, vlc
import RPi.GPIO as GPIO
import os
from Adafruit_CharLCD import Adafruit_CharLCD as LCD

'''Lecteur VLC'''
instance = vlc.Instance()
player = instance.media_player_new()
player.audio_set_volume(20)

'''Playlist'''
playlist = ['sea.mp3', 'YannTiersen.mp3', 'extremeWays.mp3']

current_track = 0
media = instance.media_new(playlist[current_track])
player.set_media(media)

'''GPIO'''
GPIO.setmode(GPIO.BCM)
STOP_BUTTON=27
NEXT_BUTTON=17
UP_BUTTON=14
DOWN_BUTTON=15
RESET_BUTTON=4
SWITCH_BUTTON=26

GPIO.setup(STOP_BUTTON, GPIO.IN)
GPIO.setup(UP_BUTTON, GPIO.IN)
GPIO.setup(DOWN_BUTTON, GPIO.IN)
GPIO.setup(RESET_BUTTON, GPIO.IN)
GPIO.setup(NEXT_BUTTON, GPIO.IN)
GPIO.setup(SWITCH_BUTTON, GPIO.IN)

'''Functions'''
def play_pause(channel):
 print('pause')
 global current_track
 if player.is_playing():
  player.pause()
  lcd.clear()
  lcd.message('Pause')
  
 else:
  player.play()
  lcd.clear()
  lcd.message(playlist[current_track].split('.')[0])
 
def next_track(channel):
 print('next')
 global current_track
 global k
 k = 0
 if current_track + 1 < len(playlist):
  current_track = current_track + 1
 else:
  current_track = 0
 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 lcd.message(playlist[current_track].split('.')[0])

def reset_track(channel):
 print('reset')
 global current_track
 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 lcd.message(playlist[current_track].split('.')[0])

def shutdown(channel):
 print('shutdown')
 lcd.clear()
 lcd.message('Arret en cours')
 player.pause()
 time.sleep(1)
 GPIO.cleanup()
 time.sleep(3)
 os.system('sudo shutdown -h now')

def volume_up(channel):
 print('up')
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = min(1000, current_volume + 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.message(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 lcd.message(playlist[current_track].split('.')[0])

def volume_down(channel):
 print('down')
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = max(0, current_volume - 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.message(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 lcd.message(playlist[current_track].split('.')[0])
 
 
'''Function & GPIO'''
GPIO.add_event_detect(STOP_BUTTON, GPIO.FALLING, callback = play_pause, bouncetime = 300)
GPIO.add_event_detect(UP_BUTTON, GPIO.FALLING, callback = volume_up, bouncetime = 300)
GPIO.add_event_detect(DOWN_BUTTON, GPIO.FALLING, callback = volume_down, bouncetime = 300)
GPIO.add_event_detect(RESET_BUTTON, GPIO.FALLING, callback = reset_track, bouncetime = 300)
GPIO.add_event_detect(NEXT_BUTTON, GPIO.FALLING, callback = next_track, bouncetime = 300)
GPIO.add_event_detect(SWITCH_BUTTON, GPIO.FALLING, callback = shutdown, bouncetime = 300)

'''Boucle principale'''

lcd = LCD()
lcd.clear()
lcd.message(playlist[current_track].split('.')[0])

try:
 k = 0
 player.play()
 while True:
  time.sleep(1)
  length = player.get_length()/1000
  
  if k > length:
   k = 0
  
  if not player.is_playing() and k == 0:
   next_track(0)
   k = 0
   
  if player.is_playing():
   k = k+1
  print(length, k)
  
except KeyboardInterrupt:
 GPIO.cleanup()
 player.stop()
  
