import time
import glob, random, sys, vlc
import RPi.GPIO as GPIO
import os
from RPLCD.gpio import CharLCD

'''Lecteur VLC'''
instance = vlc.Instance()
player = instance.media_player_new()
player.audio_set_volume(20)

'''Playlist'''
playlist = ['5.Bruit_de_mer.Joachim.mp3', '2.Comptine_d_un_autre_ete.Sophie.mp3', '42.Extreme_Ways.Jean-Baptiste.mp3']

current_track = 0
media = instance.media_new(playlist[current_track])
player.set_media(media)

'''GPIO'''
GPIO.setmode(GPIO.BCM)
STOP_BUTTON=27
NEXT_BUTTON=17
UP_BUTTON=14
DOWN_BUTTON=15
RESET_BUTTON=4
SWITCH_BUTTON=26

GPIO.setup(STOP_BUTTON, GPIO.IN)
GPIO.setup(UP_BUTTON, GPIO.IN)
GPIO.setup(DOWN_BUTTON, GPIO.IN)
GPIO.setup(RESET_BUTTON, GPIO.IN)
GPIO.setup(NEXT_BUTTON, GPIO.IN)
GPIO.setup(SWITCH_BUTTON, GPIO.IN)

'''Functions Affichage'''
def DisplaySongInfos():
 global current_track
 number, title, guest, _ = playlist[current_track].replace('_', ' ').split('.')

 lcd.cursor_pos = (0, 0)
 lcd.write_string(f'{number} - {title}')
 lcd.cursor_pos = (1, 0)
 lcd.write_string(f'{guest}')
 
 if len(f'{number} - {title}') > 16:
  Line0 = f'{number} - {title}    '
 else:
  Line0 = f'{number} - {title}'

 if len(f'{guest}') > 16:
  Line1 = f'{guest}    '
 else:
  Line1 = f'{guest}'

  return Line0, Line1
  
def scrollOneLine(line, string):
 if len(string) > 16:
  string = string + "      "
  lcd.cursor_pos = (line, 0)
  lcd.write_string(string)

'''Functions Boutons'''
def play_pause(channel):
 global current_track
 if player.is_playing():
  player.pause()
  lcd.clear()
  lcd.write_string('Pause')
  
 else:
  player.play()
  lcd.clear()
  DisplaySongInfos()
 
def next_track(channel):
 global current_track
 global k
 k = 0
 if current_track + 1 < len(playlist):
  current_track = current_track + 1
 else:
  current_track = 0
 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 DisplaySongInfos()

def reset_track(channel):
 global current_track
 media = instance.media_new(playlist[current_track])
 player.set_media(media)
 volume = player.audio_get_volume()
 player.audio_set_volume(volume)
 player.play()
 lcd.clear()
 DisplaySongInfos()

def shutdown(channel):
 lcd.clear()
 lcd.write_string('Arret en cours')
 player.pause()
 time.sleep(1)
 GPIO.cleanup()
 time.sleep(3)
 os.system('sudo shutdown -h now')

def volume_up(channel):
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = min(1000, current_volume + 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.write_string(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 DisplaySongInfos()

def volume_down(channel):
 global current_track
 current_volume = player.audio_get_volume()
 new_volume = max(0, current_volume - 5)
 player.audio_set_volume(new_volume)
 
 lcd.clear()
 lcd.write_string(f'Volume : {new_volume}')
 time.sleep(2)
 lcd.clear()
 DisplaySongInfos()
 
 
'''Function & GPIO'''
GPIO.add_event_detect(STOP_BUTTON, GPIO.FALLING, callback = play_pause, bouncetime = 300)
GPIO.add_event_detect(UP_BUTTON, GPIO.FALLING, callback = volume_up, bouncetime = 300)
GPIO.add_event_detect(DOWN_BUTTON, GPIO.FALLING, callback = volume_down, bouncetime = 300)
GPIO.add_event_detect(RESET_BUTTON, GPIO.FALLING, callback = reset_track, bouncetime = 300)
GPIO.add_event_detect(NEXT_BUTTON, GPIO.FALLING, callback = next_track, bouncetime = 300)
GPIO.add_event_detect(SWITCH_BUTTON, GPIO.FALLING, callback = shutdown, bouncetime = 300)

'''Boucle principale'''

lcd = CharLCD(pin_rs=9, pin_e=11, pins_data=[5, 6, 13, 23], numbering_mode = GPIO.BCM, auto_linebreaks = False)
lcd.clear()

try:
 k = 0
 player.play()
 while True:
  if k in [0, 0.5]:
   Line0, Line1 = DisplaySongInfos()
  time.sleep(0.5)
  if len(Line0) > 16:
   Line0 = Line0[1:] + Line0[0]
  if len(Line1) > 16:
   Line1 = Line1[2:] + Line1[0]

  scrollOneLine(0, Line0)
  scrollOneLine(1, Line1)
  length = player.get_length()/1000
  
  if k >= int(length):
   k = 0
  
  if not player.is_playing() and k == 0:
   next_track(0)
   k = 0
   
  if player.is_playing():
   k = k+0.5

  
except KeyboardInterrupt:
 GPIO.cleanup()
 player.stop()
  
